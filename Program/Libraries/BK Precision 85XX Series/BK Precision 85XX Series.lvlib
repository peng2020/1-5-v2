﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="18008000">
	<Property Name="Instrument Driver" Type="Str">True</Property>
	<Property Name="NI.Lib.DefaultMenu" Type="Str">dir.mnu</Property>
	<Property Name="NI.Lib.Description" Type="Str">LabVIEW Plug and Play instrument driver for B&amp;K Precision 85XX Series DC Electronic Loads.</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">'!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*#!!!*Q(C=\&gt;1R4ON!%-&lt;R$`1+WB25.%_ZQFQB6UC&gt;&lt;G[!5N"1ZAJTB@25+?CI*A5&amp;$6+OE#O%P]@$?QAE1A%33.D:9(_\/`O,&lt;3T6&gt;C&lt;^U?(9NHVXIZ^+WWV^BK.KP8@`^PHU/8X68`0LL!\';G0XW`H`:_O$[R`R[^XNN`_X`W@X(^E9=XVUU"&gt;PUEF*;5E,GN/M@NUBS:-]S:-]S:-]S)-]S)-]S)0=S:X=S:X=S:X=S)X=S)X=S)W]&gt;H+2CVTE8*6E]73BJ'B3)"E-2=F8YEE]C3@R=+L%EXA34_**0!R2YEE]C3@R*"[G+@%EHM34?")0J5K3N:0D34S56_!*0)%H]!1?FF4A#1$"9E(BI!A-":X"1?!*0)'(1Q7?Q".Y!E`AI6O"*`!%HM!4?*B36S6+-_TE?#ADR_.Y()`D=4S5FO.R0)\(]4A?FJ0D=4Q/QFH1+1Z"TC2HA(0C?"Q0@_2Y()`D=4S/B[[[1VZ8:N!-/TE?QW.Y$)`B-4S5E/%R0)&lt;(]"A?SMLQ'"\$9XA-$UP*]"A?QW.!D%6:8E9R9[)RS!A-$Z^[7KTO5J4%;O^`T@&amp;"64_![A&gt;,`=#I(Q4V$6&lt;@/05.56^I^1658RDV$V&lt;`%$61P&lt;#[I(KA^HTP;"P;GL;C,7FTWIQWJ5W'K:]]=,`@;\@&lt;;&lt;0:;,V?;\6;;&lt;F=;D[@;T;&lt;;4K&gt;;D+:`(N&lt;`75@NZ/8\[7\_`0&amp;Y]XFYO\KYP&lt;RZG%RH!`Z.?/_KIXPJ?`Q&lt;N3J$C^?]VSD*[@N@0)!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">402685952</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="Public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="Action-Status" Type="Folder">
			<Item Name="Action-Status.mnu" Type="Document" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Public/Action-Status/Action-Status.mnu"/>
			<Item Name="Get Instrument Status.vi" Type="VI" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Public/Action-Status/Get Instrument Status.vi"/>
			<Item Name="Input Control.vi" Type="VI" URL="../Public/Action-Status/Input Control.vi"/>
			<Item Name="Read Display Values.vi" Type="VI" URL="../Public/Action-Status/Read Display Values.vi"/>
			<Item Name="Recall List File.vi" Type="VI" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Public/Action-Status/Recall List File.vi"/>
			<Item Name="Remote Sensing State.vi" Type="VI" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Public/Action-Status/Remote Sensing State.vi"/>
			<Item Name="Save List File.vi" Type="VI" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Public/Action-Status/Save List File.vi"/>
			<Item Name="Software Trigger.vi" Type="VI" URL="../Public/Action-Status/Software Trigger.vi"/>
			<Item Name="Read Display Values custom 2.vi" Type="VI" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Public/Action-Status/Read Display Values custom 2.vi"/>
			<Item Name="Read Display Values custom w voltage.vi" Type="VI" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Public/Action-Status/Read Display Values custom w voltage.vi"/>
			<Item Name="Read Display Values custom.vi" Type="VI" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Public/Action-Status/Read Display Values custom.vi"/>
			<Item Name="Read Display Values 3.vi" Type="VI" URL="../Public/Action-Status/Read Display Values 3.vi"/>
		</Item>
		<Item Name="Configure" Type="Folder">
			<Item Name="Configure.mnu" Type="Document" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Public/Configure/Configure.mnu"/>
			<Item Name="Configure Battery Min Volt.vi" Type="VI" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Public/Configure/Configure Battery Min Volt.vi"/>
			<Item Name="Configure Comm Address.vi" Type="VI" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Public/Configure/Configure Comm Address.vi"/>
			<Item Name="Configure Constant Current.vi" Type="VI" URL="../Public/Configure/Configure Constant Current.vi"/>
			<Item Name="Configure Constant Power.vi" Type="VI" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Public/Configure/Configure Constant Power.vi"/>
			<Item Name="Configure Constant Resistance.vi" Type="VI" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Public/Configure/Configure Constant Resistance.vi"/>
			<Item Name="Configure Constant Voltage.vi" Type="VI" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Public/Configure/Configure Constant Voltage.vi"/>
			<Item Name="Configure List Name.vi" Type="VI" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Public/Configure/Configure List Name.vi"/>
			<Item Name="Configure List Operation.vi" Type="VI" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Public/Configure/Configure List Operation.vi"/>
			<Item Name="Configure List Partition.vi" Type="VI" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Public/Configure/Configure List Partition.vi"/>
			<Item Name="Configure List Step Numbers.vi" Type="VI" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Public/Configure/Configure List Step Numbers.vi"/>
			<Item Name="Configure List Step Parameters.vi" Type="VI" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Public/Configure/Configure List Step Parameters.vi"/>
			<Item Name="Configure Load Function.vi" Type="VI" URL="../Public/Configure/Configure Load Function.vi"/>
			<Item Name="Configure Load On Timer.vi" Type="VI" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Public/Configure/Configure Load On Timer.vi"/>
			<Item Name="Configure Max Input.vi" Type="VI" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Public/Configure/Configure Max Input.vi"/>
			<Item Name="Configure Mode.vi" Type="VI" URL="../Public/Configure/Configure Mode.vi"/>
			<Item Name="Configure Remote Sensing.vi" Type="VI" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Public/Configure/Configure Remote Sensing.vi"/>
			<Item Name="Configure Transient.vi" Type="VI" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Public/Configure/Configure Transient.vi"/>
			<Item Name="Configure Trigger Source.vi" Type="VI" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Public/Configure/Configure Trigger Source.vi"/>
		</Item>
		<Item Name="Data" Type="Folder">
			<Item Name="Data.mnu" Type="Document" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Public/Data/Data.mnu"/>
			<Item Name="Read Battery Min Volt.vi" Type="VI" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Public/Data/Read Battery Min Volt.vi"/>
			<Item Name="Read Constant Current.vi" Type="VI" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Public/Data/Read Constant Current.vi"/>
			<Item Name="Read Constant Power.vi" Type="VI" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Public/Data/Read Constant Power.vi"/>
			<Item Name="Read Constant Resistance.vi" Type="VI" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Public/Data/Read Constant Resistance.vi"/>
			<Item Name="Read Constant Voltage.vi" Type="VI" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Public/Data/Read Constant Voltage.vi"/>
			<Item Name="Read List Name.vi" Type="VI" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Public/Data/Read List Name.vi"/>
			<Item Name="Read List Operation.vi" Type="VI" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Public/Data/Read List Operation.vi"/>
			<Item Name="Read List Partition.vi" Type="VI" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Public/Data/Read List Partition.vi"/>
			<Item Name="Read List Step Numbers.vi" Type="VI" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Public/Data/Read List Step Numbers.vi"/>
			<Item Name="Read List Step Parameters.vi" Type="VI" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Public/Data/Read List Step Parameters.vi"/>
			<Item Name="Read Load Function.vi" Type="VI" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Public/Data/Read Load Function.vi"/>
			<Item Name="Read Load On Timer.vi" Type="VI" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Public/Data/Read Load On Timer.vi"/>
			<Item Name="Read Max Input.vi" Type="VI" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Public/Data/Read Max Input.vi"/>
			<Item Name="Read Mode.vi" Type="VI" URL="../Public/Data/Read Mode.vi"/>
			<Item Name="Read Transient.vi" Type="VI" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Public/Data/Read Transient.vi"/>
			<Item Name="Read Trigger Source.vi" Type="VI" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Public/Data/Read Trigger Source.vi"/>
		</Item>
		<Item Name="Utility" Type="Folder">
			<Item Name="Utility.mnu" Type="Document" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Public/Utility/Utility.mnu"/>
			<Item Name="Delay.vi" Type="VI" URL="../Public/Utility/Delay.vi"/>
			<Item Name="Error Query.vi" Type="VI" URL="../Public/Utility/Error Query.vi"/>
			<Item Name="Get ID.vi" Type="VI" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Public/Utility/Get ID.vi"/>
			<Item Name="Revision Query.vi" Type="VI" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Public/Utility/Revision Query.vi"/>
		</Item>
		<Item Name="dir.mnu" Type="Document" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Public/dir.mnu"/>
		<Item Name="Close.vi" Type="VI" URL="../Public/Close.vi"/>
		<Item Name="Initialize.vi" Type="VI" URL="../Public/Initialize.vi"/>
		<Item Name="VI Tree.vi" Type="VI" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Public/VI Tree.vi"/>
	</Item>
	<Item Name="Private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="Checksum.vi" Type="VI" URL="../Private/Checksum.vi"/>
		<Item Name="Control Mode.vi" Type="VI" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Private/Control Mode.vi"/>
		<Item Name="Four Bytes to Number.vi" Type="VI" URL="../Private/Four Bytes to Number.vi"/>
		<Item Name="Number to Four Bytes.vi" Type="VI" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Private/Number to Four Bytes.vi"/>
		<Item Name="Number to Two Bytes.vi" Type="VI" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Private/Number to Two Bytes.vi"/>
		<Item Name="Serial Read.vi" Type="VI" URL="../Private/Serial Read.vi"/>
		<Item Name="Serial Write.vi" Type="VI" URL="../Private/Serial Write.vi"/>
		<Item Name="Two Bytes to Number.vi" Type="VI" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Private/Two Bytes to Number.vi"/>
		<Item Name="Add Checksum.vi" Type="VI" URL="../Private/Add Checksum.vi"/>
		<Item Name="flush.vi" Type="VI" URL="../Private/flush.vi"/>
		<Item Name="Read Bar Code.vi" Type="VI" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Private/Read Bar Code.vi"/>
		<Item Name="send byte array.vi" Type="VI" URL="../Private/send byte array.vi"/>
		<Item Name="send command and read.vi" Type="VI" URL="../Private/send command and read.vi"/>
		<Item Name="Wait for bytes.vi" Type="VI" URL="../Private/Wait for bytes.vi"/>
	</Item>
</Library>
