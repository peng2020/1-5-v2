﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Name="Template - Generic.lvproj" Type="Project" LVVersion="12008004" URL="/&lt;instrlib&gt;/_Template - Generic/Template - Generic.lvproj">
	<Property Name="CCSymbols" Type="Str"></Property>
	<Property Name="Instrument Driver" Type="Str">True</Property>
	<Property Name="NI.Project.Description" Type="Str">This project is used by developers to edit API and example files for LabVIEW Plug and Play instrument drivers.</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="CCSymbols" Type="Str">OS,Win;CPU,x86;</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Examples" Type="Folder">
			<Item Name="BK Precision 85XX.bin3" Type="Document" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Examples/BK Precision 85XX.bin3"/>
			<Item Name="BK Precision 85XX Configure Dynamic List Steps.vi" Type="VI" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Examples/BK Precision 85XX Configure Dynamic List Steps.vi"/>
			<Item Name="BK Precision 85XX Internal Battery Test.vi" Type="VI" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Examples/BK Precision 85XX Internal Battery Test.vi"/>
			<Item Name="BK Precision 85XX Monitor Display Reading.vi" Type="VI" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Examples/BK Precision 85XX Monitor Display Reading.vi"/>
			<Item Name="BK Precision 85XX Run Constant Current Mode Operation.vi" Type="VI" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Examples/BK Precision 85XX Run Constant Current Mode Operation.vi"/>
			<Item Name="BK Precision 85XX Run Continuous Transient.vi" Type="VI" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Examples/BK Precision 85XX Run Continuous Transient.vi"/>
		</Item>
		<Item Name="BK Precision 85XX Series.lvlib" Type="Library" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/BK Precision 85XX Series.lvlib"/>
		<Item Name="Read Display Values 3.vi" Type="VI" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Public/Action-Status/Read Display Values 3.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="instr.lib" Type="Folder">
				<Item Name="Add Checksum.vi" Type="VI" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Private/Add Checksum.vi"/>
				<Item Name="send byte array.vi" Type="VI" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Private/send byte array.vi"/>
				<Item Name="flush.vi" Type="VI" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Private/flush.vi"/>
				<Item Name="Wait for bytes.vi" Type="VI" URL="/&lt;instrlib&gt;/BK Precision 85XX Series/Private/Wait for bytes.vi"/>
			</Item>
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="VISA Configure Serial Port" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port"/>
				<Item Name="VISA Configure Serial Port (Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Instr).vi"/>
				<Item Name="VISA Configure Serial Port (Serial Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Serial Instr).vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="VISA Flush IO Buffer Mask.ctl" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Flush IO Buffer Mask.ctl"/>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
